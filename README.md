# yoko

An AI framework that uses human rules of thumb, clever bits of math and voice commands to turn unstructured data into structured insights.